"""
PS WITH PROTONS - matching chromaticity with pole face windings 
- although V_{RF, tot} = 200 kV for the LHCPILOT cycle is too much for one cavity, should work for our 
tracking purposes

by Elias Waagaard 
"""
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json

optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-ps'

# Initiate MADX sequence, and call the relevant madx files and macros  
madx = Madx()
madx.call("{}/_scripts/macros.madx".format(optics))
madx.call("{}/scenarios/lhc/1_flat_bottom/ps_fb_lhc.beam".format(optics))   
madx.input('''BRHO      := BEAM->PC * 3.3356;''')
madx.call("{}/ps_mu.seq".format(optics))
madx.call("{}/ps_ss.seq".format(optics))
madx.call("{}/scenarios/lhc/1_flat_bottom/ps_fb_lhc.str".format(optics))
madx.call('../PS_sequence/PS_match_tunes_and_chroma.madx')
madx.use(sequence='ps')

# Put in values for PS protons to LHC: https://acc-models.web.cern.ch/acc-models/ps/2022/scenarios/lhc/

# When we perform matching, recall the MADX convention that all chromatic functions are multiplied by relativistic beta factor
# Thus, when we match for a given chromaticity, need to account for this factor to get correct value in Xsuite and PTC
beta0 = madx.sequence['ps'].beam.beta 
madx.input("qx = 6.21")
madx.input("qy = 6.245")
madx.input(f"qpx = 0.7273839232/{beta0}")
madx.input(f"qpy = -2.871405047/{beta0}")

madx.use("ps")
madx.input("seqedit, sequence=PS;")
madx.input("flatten;")
madx.input("endedit;")
madx.use("ps")
madx.input("select, flag=makethin, slice=5, thick=false;")
madx.input("makethin, sequence=ps, style=teapot, makedipedge=True;")
madx.use('ps')
madx.input("exec, match_tunes_and_chroma(qx, qy, qpx, qpy);")

madx.use(sequence = 'ps')
twiss_thin = madx.twiss()

#%% Test with PTC-Twiss 
madx.input('''        
dp = 0;
order = 2;
ptc_create_universe;
ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
select, flag=ptc_twiss, clear;
select, flag=ptc_twiss, column=name,keyword,s,x,px,beta11,alfa11,beta22,alfa22,disp1,disp2,mu1,mu2,energy,l,angle,K1L,K2L,K3L,HKICK,SLOT_ID;    

use, sequence=SPS;
ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, normal;
ptc_end;
           ''')
ptc_table = madx.table.ptc_twiss_summary
ptc_twiss = madx.ptc_twiss


#%% XSUITE TRACKER AND TWISS 

# Perform Twiss command with MADX
madx.use(sequence='ps')
line = xt.Line.from_madx_sequence(madx.sequence['ps'])
madx_beam = madx.sequence['ps'].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample

# Perform Twiss command from tracker and save Xtrack sequence in json format
line.build_tracker()
twiss_xtrack = line.twiss(method='4d')  


print("\nPROTONS: XTRACK vs MADX sequence:")
print("MAD-X PTC:    " f"Qx  = {ptc_table['q1'][0]:.8f}"                     f"   Qy = {ptc_table['q2'][0]:.8f}")
print("MAD-X thin:   " f"Qx  = {twiss_thin.summary['q1']:.8f}"            f"   Qy  = {twiss_thin.summary['q2']:.8f}")
print("Xsuite:       " f"Qx  = {twiss_xtrack['qx']:.8f}"                  f"   Qy  = {twiss_xtrack['qy']:.8f}\n")
print("\nMAD-X PTC:    " f"Q'x = {ptc_table['dq1'][0]:.8f}"                     f"  Q'y = {ptc_table['dq2'][0]:.8f}")
print("MAD-X thin:   " f"Q'x = {twiss_thin.summary['dq1']*beta0:.7f}"     f"   Q'y = {twiss_thin.summary['dq2']*beta0:.7f}")
print("Xsuite:       " f"Q'x = {twiss_xtrack['dqx']:.7f}"                 f"   Q'y = {twiss_xtrack['dqy']:.7f}\n")
print("\nMAD-X PTC:    " f"alpha_p = {ptc_table['alpha_c'][0]:.8f}")
print("MAD-X thin:   " f"alpha_p = {twiss_thin.summary.alfa:.7f}")
print("Xsuite:       " f"alpha_p = {twiss_xtrack['momentum_compaction_factor']:.7f}")

#%% SET CAVITY VOLTAGE - with info from Alexandre Lasheen
# Ions: 10 MHz cavities: 200 kV for LHCPILOT, h=16
# In the ps_ss.seq, the 10 MHz cavities are PR_ACC10 - there seems to be 12 of them in the straight sections
harmonic_nb = 16
nn = 'pa.c10.11'  # for now test the first of the RF cavities 
V_RF = 200  # kV - in reality too high for one cavity but should suffice for our tracking purposes

# MADX sequence 
madx.sequence.ps.elements[nn].lag = 0
madx.sequence.ps.elements[nn].volt = V_RF*1e-3*particle_sample.q0 # different convention between madx and xsuite
madx.sequence.ps.elements[nn].freq = madx.sequence['ps'].beam.freq0*harmonic_nb

# Xsuite sequence 
line[nn].lag = 0  # 0 if below transition
line[nn].voltage =  V_RF*1e3 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['ps'].beam.freq0*1e6*harmonic_nb

#%% Check Twiss commands that they still make sense
madx.use(sequence = 'ps')
twiss_thin_RF = madx.twiss()

twiss_xtrack_RF = line.twiss()  

print("\nPROTONS WITH RF: XTRACK vs MADX sequence:")
print("MAD-X thin:   " f"Qx  = {twiss_thin_RF.summary['q1']:.8f}"            f"   Qy  = {twiss_thin_RF.summary['q2']:.8f}")
print("Xsuite:       " f"Qx  = {twiss_xtrack_RF['qx']:.8f}"                  f"   Qy  = {twiss_xtrack_RF['qy']:.8f}\n")
print("MAD-X thin:   " f"Q'x = {twiss_thin_RF.summary['dq1']*beta0:.7f}"     f"   Q'y = {twiss_thin_RF.summary['dq2']*beta0:.7f}")
print("Xsuite:       " f"Q'x = {twiss_xtrack_RF['dqx']:.7f}"                 f"   Q'y = {twiss_xtrack_RF['dqy']:.7f}\n")
print("MAD-X thin:   " f"alpha_p = {twiss_thin_RF.summary.alfa:.7f}")
print("Xsuite:       " f"alpha_p = {twiss_xtrack_RF['momentum_compaction_factor']:.7f}")

#%% SAVE SEQUENCES 

# Save MADX sequence
madx.command.save(sequence='ps', file='../PS_sequence/PS_2022_Protons_thin_matched_with_RF.seq', beam=True)

# Save Xsuite sequence
with open('../PS_sequence/PS_2022_Protons_matched_with_RF.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)

