"""
LEIR WITH PB IONS FLAT BOTTOM - matching chromaticity with sextupoles 
by Elias Waagaard 
"""
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json

optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-ps'

# Pb ion beam parameters 
m_u = 931.49410242e6  # 1 Dalton in eV/c^2 -- atomic mass unit 
atomic_mass_pb208 = 207.9766519 # Pb208 atomic mass from The AME2016 atomic mass evaluation (II).
m_ion = atomic_mass_pb208*m_u  
Z = 54
# We look at moment just before RF capture 
E_kin = 0.0042*1e9*atomic_mass_pb208 # total kinetic energy in eV per particle at injection, from LIU parameter table https://edms.cern.ch/ui/file/1420286/2/LIU-Ions_beam_parameter_table.pdf
E_tot = m_ion + E_kin

# Initiate MADX sequence, and call the relevant madx files and macros  
madx = Madx()
madx.call("{}/_scripts/macros.madx".format(optics))

# This ACC-MODEL BEAM command seems to calculate per particle - but wrong as MADX wants total energy! 

# Correct beam command tp obtain same Brho and beta0 as Hannes' and Isabelle Table 5 (https://cds.cern.ch/record/2749453)
madx.input(" \
           Beam, particle=ion, mass={}, charge={}, energy = {}; \
           DPP:=BEAM->SIGE*(BEAM->ENERGY/BEAM->PC)^2;  \
           ".format(m_ion/1e9, Z, E_tot/1e9))

# Call main sequence files and magnet strengths  
#madx.call("{}/leir.seq".format(optics))
madx.call("{}/new_leir_seq.seq".format(optics))
madx.call("{}/scenarios/nominal/2_flat_top/leir_ft_nominal.str".format(optics))
madx.call("{}/leir.dbx".format(optics))
madx.use(sequence='leir')

# Global correction of the coupling introduced by the electron cooler
madx.input('''
           use, sequence=LEIR;
           exec, global_correction;;
           ''')

# Put in values for LEIR ions: https://acc-models.web.cern.ch/acc-models/leir/2021/scenarios/nominal/
# - chromaticity at flat bottom set to 0 

# When we perform matching, recall the MADX convention that all chromatic functions are multiplied by relativistic beta factor
# Thus, when we match for a given chromaticity, need to account for this factor to get correct value in Xsuite and PTC
beta0 = madx.sequence['ps'].beam.beta 
madx.input("qx = 1.82")
madx.input("qy = 2.72")
madx.input(f"qpx = 0.0")
madx.input(f"qpy = 0.0")

# CAN PROBABLY USE SIMILAR PS MATCHING MACRO, BUT STILL NEED TO FIND A WAY TO CORRECTLY
# EXTRACT THE POLE FACE WINDINGS MULTIPOLAR COMPONENTS 