"""
SPS WITH PROTONS FLAT BOTTOM - add magnet errors 
- magnet errors: estimated from non-linear chromaticity measurements to reproduce the actual machine
- add magnet errors just like Xavier Buffat did in SPSTMCI.py in /afs/cern.ch/work/x/xbuffat/public/forKostas
- these errors are for Q26 (or Q20) protons at 26 GeV
- also matching chromaticity with sextupoles with correct weight 

by Elias Waagaard 
"""
import matplotlib.pyplot as plt

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import acc_lib
import json

optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-sps'
lhc_beam_type='q26'  # q20 or q26, if not ions

# Initiate MADX and call the matched sequence with RF
madx = Madx()
madx.call('../../SPS_sequence/SPS_2021_Protons_matched_with_RF.seq')
madx.use(sequence='sps')

# SPS Proton CHROMA VALUES: https://acc-models.web.cern.ch/acc-models/sps/2021/scenarios/lhc_proton/4_q26/
madx.input('''        
dp = 0;
order = 2;
ptc_create_universe;
ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
select, flag=ptc_twiss, clear;
select, flag=ptc_twiss, column=name,keyword,s,x,px,beta11,alfa11,beta22,alfa22,disp1,disp2,mu1,mu2,energy,l,angle,K1L,K2L,K3L,HKICK,SLOT_ID;    

use, sequence=SPS;
ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, normal;
ptc_end;
           ''')
ptc_table_thick = madx.table.ptc_twiss_summary
ptc_twiss_thick = madx.ptc_twiss

# Set tunes and chromaticity from PTC Twiss 
qx0 = 26.13
qy0 = 26.18
dq1 = madx.table.ptc_twiss_summary['dq1'][0]
dq2 = madx.table.ptc_twiss_summary['dq2'][0]

# Add the magnet errors 
madx.call('Xavier_forKostas/madx/sps/cmd/sps_setMultipoles_upto7.cmd')
madx.input('exec, set_Multipoles_26GeV;')
madx.call('Xavier_forKostas/madx/sps/cmd/sps_assignMultipoles_upto7.cmd')
madx.input('exec, AssignMultipoles;')
errtab_protons = madx.table.errtab

# Use correct tune and chromaticity matching macros
madx.call("{}/toolkit/macro.madx".format(optics))
madx.use('sps')
madx.exec(f"sps_match_tunes({qx0},{qy0});")
madx.exec("sps_define_sext_knobs();")
madx.exec(f"sps_set_chroma_weights_{lhc_beam_type}();")
madx.input(f"""match;
global, dq1={dq1};
global, dq2={dq2};
vary, name=qph_setvalue;
vary, name=qpv_setvalue;
jacobian, calls=10, tolerance=1e-25;
endmatch;""")

#%% 1) Convert MADX sequence to Xtrack line and save SPS sequence with errors, WITHOUT APERTURE

# Build Xtrack line importing MAD-X expressions
line = xt.Line.from_madx_sequence(madx.sequence['sps'],
                                  install_apertures=False
                                  )
particle_sample = xp.Particles(
        p0c = madx.sequence.sps.beam.pc*1e9,
        q0 = madx.sequence.sps.beam.charge,
        mass0 = madx.sequence.sps.beam.mass*1e9)

line.particle_ref = particle_sample

# Xsuite - reconfigure RF and correct phase - PROTONS
harmonic_nb = 4620
nn = 'actcse.31637'
V_RF = 3.0 # MV
line[nn].lag = 180  # above transition
line[nn].voltage =  V_RF*1e6 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['sps'].beam.freq0*1e6*harmonic_nb

# Save Xsuite sequence
with open('../../SPS_sequence/SPS_2021_Protons_matched_with_RF_and_magnet_errors.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)

# Test Twiss to observe apertures
line.build_tracker()
twiss_xtrack_with_errors = line.twiss()  


#%% 2) Add aperture files to the MADX sequence 

# Add aperture classes
madx.use(sequence='sps')
madx.call('{}/aperture/APERTURE_SPS_LS2_30-SEP-2020.seq'.format(optics))

# Updated beam emittances 
madx.sequence.sps.beam.exn = 2.5e-6
madx.sequence.sps.beam.eyn = 2.5e-6

#Activate the aperture for the Twiss flag to include it in Twiss command! 
madx.use(sequence='sps')
madx.input('select,flag=twiss,clear;')
madx.select(flag='twiss', column=['name','s','l',
              'lrad','angle','k1l','k2l','k3l','k1sl','k2sl','k3sl','hkick','vkick','kick','tilt',
              'betx','bety','alfx','alfy','dx','dpx','dy','dpy','mux','muy','x','y','px','py','t','pt',
              'wx','wy','phix','phiy','n1','ddx','ddy','ddpx','ddpy',
              'keyword','aper_1','aper_2','aper_3','aper_4',
              'apoff_1','apoff_2',
              'aptol_1','aptol_2','aptol_3','apertype','mech_sep'])


#%% 3) Initiate Xtrack sequence with aperture 
del line, particle_sample

# Build Xtrack line importing MAD-X expressions
line = xt.Line.from_madx_sequence(madx.sequence['sps'],
                                  install_apertures=True
                                  )
particle_sample = xp.Particles(
        p0c = madx.sequence.sps.beam.pc*1e9,
        q0 = madx.sequence.sps.beam.charge,
        mass0 = madx.sequence.sps.beam.mass*1e9)

line.particle_ref = particle_sample

# Xsuite - reconfigure RF and correct phase - PROTONS
harmonic_nb = 4620
nn = 'actcse.31637'
V_RF = 3.0 # MV
line[nn].lag = 180  # above transition
line[nn].voltage =  V_RF*1e6 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['sps'].beam.freq0*1e6*harmonic_nb

# Check apertures of line
aper_test = line.check_aperture()

# Save Xsuite sequence
with open('../../SPS_sequence/SPS_2021_Protons_matched_with_RF_and_aperture_and_magnet_errors.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)

# Test Twiss to observe apertures
line.build_tracker()
twiss_xtrack_with_errors_and_aperture = line.twiss()  

# Also do MADX command
twiss = madx.twiss()
new_pos_x, aper_neat_x = acc_lib.madx_tools.get_apertures_real(twiss)
new_pos_y, aper_neat_y = acc_lib.madx_tools.get_apertures_real(twiss, axis='vertical')

#%% Plot the beam envelope and aperture
fig = plt.figure(figsize=(10,7))
ax = acc_lib.madx_tools.plot_envelope(fig, madx, twiss)
acc_lib.madx_tools.plot_apertures_real(ax, new_pos_x, aper_neat_x)
fig.suptitle('SPS Protons with errors - horizontal aperture', fontsize=22)
fig.savefig('Plots/SPS_2021_Twiss_Protons_X_aperture.png', dpi=250)

fig2 = plt.figure(figsize=(10,7))
ax2 = acc_lib.madx_tools.plot_envelope(fig2, madx, twiss, axis='vertical')
acc_lib.madx_tools.plot_apertures_real(ax2, new_pos_y, aper_neat_y)
fig2.suptitle('SPS Protons errors - vertical aperture', fontsize=22)
fig2.savefig('Plots/SPS_2021_Twiss_Protons_Y_aperture.png', dpi=250)
