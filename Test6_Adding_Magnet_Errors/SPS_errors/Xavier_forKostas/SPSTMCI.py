import time,os,pickle,json
import numpy as np
import cupy as cp

from cpymad.madx import Madx

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

settings_file_name = open('settings.json','r')
settings = json.load(settings_file_name)
settings_file_name.close()

###############################
# Enable pyheadtail interface #
###############################

xp.enable_pyheadtail_interface()

############
# Settings #
############

gpu_device = 0
optics = settings['optics']
bunch_intensity = settings['npart']
nemitt_x=settings['neps_x']
nemitt_y=settings['neps_y']
sigma_z = settings['sigma_z']
seq_name = "sps"
QInt = int(optics)
qx0,qy0 = QInt+0.15, QInt+0.22
p0c = 26e9
cavity_name = "actcse.31632"
cavity_lag = 180
frequency = 200e6
rf_voltage = settings['V_RF']
cavity_800_name = "actcse.31637"
cavity_800_lag = 180
cavity_800_frequency = 800e6
cavity_800_voltage = settings['V_800']
n_slices_wakes = 500
limit_n_rms_z = 3
limit_z = limit_n_rms_z*sigma_z
n_part=int(1e6)
num_turns=int(5E4) # int(3E5) -> full injection plateau. However during first study most aborts occured before 5E4 turns
num_spacecharge_interactions = 540
tol_spacecharge_position = 1e-2
chroma_x = 3.0
chroma_y = 2.0
macro_size = bunch_intensity/n_part

magnetic_errors = settings['errors']
if 'SC' not in settings.keys():
    enable_SC = True
else:
    enable_SC = settings['SC']
use_wakes = True

studyName = settings['simKey']
output_dir = './'
# mode = 'frozen'
mode = 'pic'

#######################################################
# Use cpymad to load the sequence and match the tunes #
#######################################################

mad = Madx()
mad.call("/afs/cern.ch/eng/acc-models/sps/2021/sps.seq")
mad.call(f"/afs/cern.ch/eng/acc-models/sps/2021/strengths/lhc_q{optics}.str")
mad.call("/afs/cern.ch/eng/acc-models/sps/2021/toolkit/macro.madx")

mad.beam()
mad.use(seq_name)

#mad.twiss()
#tw_thick = mad.table.twiss.dframe()
#summ_thick = mad.table.summ.dframe()

mad.input("""select, flag=makethin, slice=1, thick=false;
            makethin, sequence=sps, style=teapot, makedipedge=false;""")
mad.use(seq_name)
mad.call("/afs/cern.ch/eng/acc-models/sps/2021/aperture/APERTURE_SPS_LS2_30-SEP-2020.seq")
mad.use(seq_name)
# ! START AT BWS51995 (original s=5243.0323)
mad.input("""mystart: marker;
SEQEDIT, sequence=sps;
    install, element=mystart, at=5243.0323; !equivalent to: at=BWSA.51995->l/2, from=BWSA.51995;
    flatten;
    cycle,start=mystart;
    remove, element=SPS$START;
    remove, element=SPS$END;
    remove, element=BIPMH.51634;
    remove, element=BIPMH.51634.A;
    remove, element=BIPMV.51694;
    remove, element=BIPMV.51694.A;
    flatten;
ENDEDIT;
""")
mad.use(seq_name)


if magnetic_errors:
    mad.call('./madx/sps/cmd/sps_setMultipoles_upto7.cmd')
    mad.input('exec, set_Multipoles_26GeV;')
    mad.call('./madx/sps/cmd/sps_assignMultipoles_upto7.cmd')
    mad.input('exec, AssignMultipoles;')

    errtab = mad.table.errtab

mad.exec(f"sps_match_tunes({qx0},{qy0});")
mad.exec("sps_define_sext_knobs();")
mad.exec(f"sps_set_chroma_weights_q{optics}();")
mad.input(f"""match;
global, dq1={chroma_x};
global, dq2={chroma_y};
vary, name=qph_setvalue;
vary, name=qpv_setvalue;
jacobian, calls=10, tolerance=1e-25;
endmatch;""")

mad.twiss()
tw_thin = mad.table.twiss.dframe()
summ_thin = mad.table.summ.dframe()
mad.call('./madx/ptc/ptc.macro')
mad.input('exec, PTCchroma;')

##################
# Switch context #
##################

if gpu_device is None:
   context = xo.ContextCpu()
else:
   context = xo.ContextCupy(device=gpu_device)

#################################
# Build line from MAD-X lattice #
#################################

line = xt.Line.from_madx_sequence(sequence=mad.sequence[seq_name],
           deferred_expressions=True, install_apertures=True,
           apply_madx_errors=True)
# Define reference particle
line.particle_ref = xp.Particles(p0c=p0c,mass0=xp.PROTON_MASS_EV)

################
# Switch on RF #
################

line[cavity_name].voltage = rf_voltage
line[cavity_name].lag = cavity_lag
line[cavity_name].frequency = frequency

line[cavity_800_name].voltage = cavity_800_voltage
line[cavity_800_name].lag = cavity_800_lag
line[cavity_800_name].frequency = cavity_800_frequency

############################################
# Twiss (check that the lattice is stable) #
############################################

# We make a copy of the line so that we can still insert elements in the
# original one (would be prevented by the existance of a tracker linked to the
# line).
tw = xt.Tracker(line=line.copy()).twiss()

print('XTrack tunes',tw['qx'],tw['qy'])
print('XTrack chromas',tw['dqx'],tw['dqy'])

longitudinalAperture = xt.LongitudinalLimitRect(_context=context,
                min_zeta=-1.5*limit_z,max_zeta=1.5*limit_z, min_pzeta=-1, max_pzeta = 1)
line.append_element(longitudinalAperture,'LongColl0')

#####################################
# Install spacecharge interactions) #
#####################################

# Install frozen space-charge lenses
if enable_SC:
    lprofile = xf.LongitudinalProfileQGaussian(
           number_of_particles=bunch_intensity,
           sigma_z=sigma_z,
           z0=0.,
           q_parameter=1.)

    xf.install_spacecharge_frozen(line=line,
                      particle_ref=line.particle_ref,
                      longitudinal_profile=lprofile,
                      nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                      sigma_z=sigma_z,
                      num_spacecharge_interactions=num_spacecharge_interactions,
                      tol_spacecharge_position=tol_spacecharge_position)

    # Switch to PIC or quasi-frozen
    if mode == 'frozen':
       pass # Already configured in line
    elif mode == 'quasi-frozen':
       xf.replace_spacecharge_with_quasi_frozen(
                                       line,
                                       update_mean_x_on_track=True,
                                       update_mean_y_on_track=True)
    elif mode == 'pic':
       pic_collection, all_pics = xf.replace_spacecharge_with_PIC(
           _context=context, line=line,
           n_sigmas_range_pic_x=8,
           n_sigmas_range_pic_y=8,
           nx_grid=256, ny_grid=256, nz_grid=100,
           n_lims_x=7, n_lims_y=3,
           z_range=(-3*sigma_z, 3*sigma_z))
    else:
       raise ValueError(f'Invalid mode: {mode}')

######################
# Install wakefields #
######################

if use_wakes:

   # We rescale the waketable to take into account for the difference between
   # the average beta functions (for which the table is calculated) and the beta
   # functions at the location in the lattice at which the wake element is
   # installed.
   wakefields = np.genfromtxt(f'/afs/cern.ch/work/x/xbuffat/SPSSpaceCharge/SPSTMCI-xsuite/wakes/SPS_complete_wake_model_PostLS2_Q{optics}.txt')
   if int(optics) == 20:
       avg_beta_x = 54.645
       avg_beta_y = 54.509
   elif int(optics) == 22:
       avg_beta_x = 49.706
       avg_beta_y = 49.594
   elif int(optics) == 26:
       avg_beta_x = 42.097
       avg_beta_y = 42.017
   else:
       print(optics,'is not a valid optics')
       exit()
   wakefields[:,1] *= avg_beta_x/tw['betx'][0]
   wakefields[:,2] *= avg_beta_y/tw['bety'][0]
   wakefields[:,3] *= avg_beta_x/tw['betx'][0]
   wakefields[:,4] *= avg_beta_y/tw['bety'][0]
   wakefile = 'wakefields.wake'
   np.savetxt(wakefile,wakefields,delimiter='\t')

   # Build PyHEADTAIL wakefield element
   from PyHEADTAIL.particles.slicing import UniformBinSlicer
   from PyHEADTAIL.impedances.wakes import WakeTable, WakeField
   n_slices_wakes = 500
   slicer_for_wakefields = UniformBinSlicer(n_slices_wakes, z_cuts=(-limit_z, limit_z))
   waketable = WakeTable(wakefile, ['time', 'dipole_x', 'dipole_y',
                  'quadrupole_x', 'quadrupole_y'])
   wakefield = WakeField(slicer_for_wakefields, waketable)

   # Specity that the wakefield element needs to run on CPU and that lost
   # particles need to be hidden for this element (required by PyHEADTAIL)
   wakefield.needs_cpu = True
   wakefield.needs_hidden_lost_particles = True

   # Insert element in the line
   line.insert_element(element=wakefield,name="wakefield", at_s=0)

#################
# Build Tracker #
#################

tracker = xt.Tracker(_context=context, line=line)

######################
# Generate particles #
######################

# (we choose to match the distribution without accounting for spacecharge)
tracker_sc_off = tracker.filter_elements(exclude_types_starting_with='SpaceCh')

particles = xp.generate_matched_gaussian_bunch(_context=context,
        num_particles=n_part, total_intensity_particles=bunch_intensity,
        nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
        particle_ref=tracker.particle_ref, tracker=tracker_sc_off)
particles.circumference = line.get_length() # Needed by pyheadtail

#########
# Track #
#########

def emittance(u,up,mean_delta_square,beta_gamma=1):
    mean_u_delta = cp.mean(u*delta)
    mean_up_delta = cp.mean(up*delta)
    uu = cp.mean(u*u) - mean_u_delta**2/mean_delta_square
    uup = cp.mean(u*up) - mean_u_delta*mean_up_delta/mean_delta_square
    upup = cp.mean(up*up) - mean_up_delta**2/mean_delta_square
    uu = cp.asnumpy(uu).tolist()
    uup = cp.asnumpy(uup).tolist()
    upup = cp.asnumpy(upup).tolist()
    return np.sqrt(uu*upup - uup**2)*beta_gamma

def calculate_emittances(x,px,y,py,sigma,delta,beta_gamma=1):
    delta_square = delta*delta
    mean_delta_square = cp.mean(delta_square)
    return emittance(x,px,mean_delta_square,beta_gamma=beta_gamma), emittance(y,py,mean_delta_square,beta_gamma=beta_gamma)

def get_mean(x):
    return cp.asnumpy(cp.mean(x)).tolist()

def get_sigma(x):
    return cp.asnumpy(cp.std(x)).tolist()
    
def get_max(x):
    return cp.asnumpy(cp.max(cp.abs(x))).tolist()

gamma0 = cp.asnumpy(particles.gamma0[0]).tolist()
beta0 = cp.asnumpy(particles.beta0[0]).tolist()
betagamma = beta0*gamma0
abortNextPrint = False
tbt_dict = {'turn':[], 'intensity':[], 'neps_x':[], 'neps_y':[], 
    'mean_x':[], 'mean_y':[], 'mean_px':[], 'mean_py':[],
    'mean_sigma':[], 'mean_delta':[],'max_sigma':[], 'max_delta':[],'rms_sigma':[], 'rms_delta':[],
    'slices_z_centers': [], 'slices_mean_x': [],
    'slices_mean_y': [], 'slices_mean_dp':[], 'n_macroparticles_per_slice':[]}
#z_tbt = np.zeros((num_turns,n_part),dtype=float)
#delta_tbt = np.zeros_like(z_tbt)
for turn in range(num_turns):
    time0 = time.time()
    tracker.track(particles)
    indx_alive = np.where( particles.state == 1)
    x = particles.x[indx_alive]
    px = particles.px[indx_alive]
    y = particles.y[indx_alive]
    py = particles.py[indx_alive]
    sigma = particles.zeta[indx_alive]
    delta = particles.delta[indx_alive]
    neps_x, neps_y = calculate_emittances(x, px, y, py, sigma, delta, betagamma)
    n_mp_alive = len(indx_alive[0])
    intensity = macro_size * n_mp_alive
    tbt_dict['turn'].append(turn)
    tbt_dict['intensity'].append(intensity)
    tbt_dict['neps_x'].append(neps_x)
    tbt_dict['neps_y'].append(neps_y)
    tbt_dict['mean_x'].append(get_mean(x))
    tbt_dict['mean_y'].append(get_mean(y))
    tbt_dict['mean_px'].append(get_mean(px))
    tbt_dict['mean_py'].append(get_mean(py))
    tbt_dict['mean_sigma'].append(get_mean(sigma))
    tbt_dict['mean_delta'].append(get_mean(delta))
    tbt_dict['max_sigma'].append(get_max(sigma))
    tbt_dict['max_delta'].append(get_max(delta))
    tbt_dict['rms_sigma'].append(get_sigma(sigma))
    tbt_dict['rms_delta'].append(get_sigma(delta))
    
    #z_tbt[turn,:] = cp.asnumpy(sigma)
    #delta_tbt[turn,:] = cp.asnumpy(delta)
    
    # TODO
    #slices = particles.get_slices(particles)
    #tbt_dict['slices_z_centers'].append(slices.z_centers)
    #tbt_dict['slices_mean_x'].append(slices.mean_x)
    #tbt_dict['slices_mean_y'].append(slices.mean_y)
    #tbt_dict['slices_mean_dp'].append(slices.mean_dp)
    #tbt_dict['n_macroparticles_per_slice'].append(slices.n_macroparticles_per_slice)
    
    if not abortNextPrint:
        if neps_x > 1.2*tbt_dict['neps_x'][0]:
            print('horizontal emittance growth by more than 20%, requesting abort')
            with open(os.path.join(output_dir,'abort.txt'), 'a') as fid:
                fid.write(f'Aborted at turn {turn} due to large horizontal emittance')
            abortNextPrint = True
        if neps_y > 1.2*tbt_dict['neps_x'][0]:
            print('vertical emittance growth by more than 20%, requesting abort')
            with open(os.path.join(output_dir,'abort.txt'), 'a') as fid:
                fid.write(f'Aborted at turn {turn} due to large vertical emittance')
            abortNextPrint = True
        if intensity < 0.8*tbt_dict['intensity'][0]:
            print('Losses about 20%, requesting abort')
            with open(os.path.join(output_dir,'abort.txt'), 'a') as fid:
                fid.write(f'Aborted at turn {turn} due to large losses')
            abortNextPrint = True
    
    if turn % 1000 == 0:
        with open(os.path.join(output_dir,'tbt.pkl'), 'wb') as fid:
            pickle.dump(tbt_dict, fid)
        #with open(os.path.join(output_dir,'log_phase_space.pkl'), 'wb') as fid:
        #    pickle.dump([z_tbt,delta_tbt], fid)
        if abortNextPrint:
            print('Abort')
            break
    
    print(turn,time.time()-time0)
with open(os.path.join(output_dir,'tbt.pkl'), 'wb') as fid:
    pickle.dump(tbt_dict, fid)
    
