import os,json,shutil,stat,sys

python_script_source_path = '/afs/cern.ch/work/x/xbuffat/SPSSpaceCharge/SPSTMCI-xsuite/SPSTMCI.py'
python_script_name = os.path.basename(python_script_source_path)

for emit in [float(sys.argv[4])]:
    settings = {}
    settings['npart'] = float(sys.argv[3])
    settings['neps_x'] = emit
    settings['neps_y'] = emit
    settings['klof'] = 0.0
    settings['errors'] = True
    settings['SC'] = False
    settings['sigma_z'] = float(sys.argv[2])
    if sys.argv[1] == '26':
        settings['optics'] = 26;settings['V_RF'] = 1.4E6
    elif sys.argv[1] == '22':
        settings['optics'] = 22;settings['V_RF'] = 2.7E6
    elif sys.argv[1] == '20':
        settings['optics'] = 20;settings['V_RF'] = 4.5E6
    else:
        print(sys.argv[1],'is not a valid optic')
        exit()
    settings['V_800'] = 0.1*settings['V_RF']


    simKey = f"SPSTMCI-xsuite3_Q{settings['optics']}"
    for setting in ['npart','neps_x','neps_y','sigma_z','V_RF','V_800']:
        simKey += f'-{setting}{settings[setting]:.3E}'
    if settings['errors']:
        simKey += f'-NLerrors'
    if not settings['SC']:
        simKey += f'-noSC'

    settings['simKey'] = simKey
    settings['output_directory_afs'] = os.path.join('/afs/cern.ch/work/x/xbuffat/SPSSpaceCharge/SPSTMCI-xsuite/output',settings['simKey'])
    settings['output_directory_eos'] = os.path.join('/eos/project/b/beaminstabilitydata/PyHEADTAIL/SPSSpaceCharge/',settings['simKey'])

    turnbyturn_file_name = 'tbt.pkl'
    turnbyturn_path_eos = os.path.join(settings['output_directory_eos'],turnbyturn_file_name)

    settings['relaunch'] = False
    if os.path.exists(settings['output_directory_afs']):
        print(settings['output_directory_afs'],'already exists')
        if os.path.exists(turnbyturn_path_eos):
            print(turnbyturn_path_eos,'already exists')
            #print('relaunching')
            #settings['relaunch'] = True
        else:
            print(turnbyturn_path_eos,'does not exists: Trying again')
            shutil.rmtree(settings['output_directory_afs'])
            if os.path.exists(settings['output_directory_eos']):
                shutil.rmtree(settings['output_directory_eos'])
            settings['relaunch'] = False
            #print('abort')
            #continue
    if not settings['relaunch'] and os.path.exists(settings['output_directory_eos']):
        print(settings['output_directory_eos'],'already exists')
        continue

    job_file_name = os.path.join(settings['output_directory_afs'],'SPSSpaceCharge.job')
    if not settings['relaunch']:
        os.makedirs(settings['output_directory_afs'])
        python_script_path = os.path.join(settings['output_directory_afs'],python_script_name)
        shutil.copy(python_script_source_path,python_script_path)

        settings_file_path = os.path.join(settings['output_directory_afs'],'settings.json')
        settings_file_name = os.path.basename(settings_file_path)
        settings_file = open(settings_file_path,'w')
        json.dump(settings,settings_file)
        settings_file.close()

        bash_script_path = os.path.join(settings['output_directory_afs'],'SPSSpaceCharge.sh')
        bash_script_name = os.path.basename(bash_script_path)
        bash_script = open(bash_script_path,'w')
        bash_script.write(
f'''#!/bin/bash\n
echo 'sourcing environement'
source /afs/cern.ch/user/x/xbuffat/miniconda3_V100/bin/activate
echo 'Running job'
python {python_script_name} 1> out.txt 2> err.txt
echo 'Done'
xrdcp -f {turnbyturn_file_name} {turnbyturn_path_eos}
xrdcp -f out.txt {os.path.join(settings['output_directory_eos'],"out.txt")}
xrdcp -f err.txt {os.path.join(settings['output_directory_eos'],"err.txt")}
xrdcp -f abort.txt {os.path.join(settings['output_directory_eos'],"abort.txt")}
''')
        bash_script.close()
        os.chmod(bash_script_path, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IROTH | stat.S_IXOTH)

        job_file = open(job_file_name,'w')
        job_file.write(
f'''executable        = {bash_script_path}
transfer_input_files  = {python_script_path},{settings_file_path}
output                = {os.path.join(settings['output_directory_afs'],"SPSSpaceCharge.out")}
error                 = {os.path.join(settings['output_directory_afs'],"SPSSpaceCharge.err")}
log                   = {os.path.join(settings['output_directory_afs'],"SPSSpaceCharge.log")}
request_CPUs = 1
request_GPUs = 1
+MaxRuntime           = 1209500
requirements = regexp("V100", TARGET.CUDADeviceName) || regexp("A100", TARGET.CUDADeviceName)
queue''')
        job_file.close()
#+JobFlavour = "nextweek"
#+MaxRuntime           = 2592000
#+MaxRuntime           = 1209500

        shutil.copytree(settings['output_directory_afs'],settings['output_directory_eos'])

    os.system(f'condor_submit -spool {job_file_name}')
    #os.system(f'condor_submit {job_file_name}')

