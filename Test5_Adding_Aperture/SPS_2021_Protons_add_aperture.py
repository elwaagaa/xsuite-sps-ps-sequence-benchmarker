"""
SPS PROTONS - ADD APERTURE TO SEQUENCE FILE

by Elias Waagaard 
"""
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import json

import xobjects as xo
import xtrack as xt
import xpart as xp

import acc_lib 

# Define optics
optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-sps'

# Initiate MADX and call the matched sequence with RF
madx = Madx()
madx.call('../SPS_sequence/SPS_2021_Protons_matched_with_RF.seq')

# Add aperture classes
madx.use(sequence='sps')
madx.call('{}/aperture/APERTURE_SPS_LS2_30-SEP-2020.seq'.format(optics))

# Updated beam emittances 
madx.sequence.sps.beam.exn = 2.5e-6
madx.sequence.sps.beam.eyn = 2.5e-6

#Activate the aperture for the Twiss flag to include it in Twiss command! 
madx.use(sequence='sps')
madx.input('select,flag=twiss,clear;')
madx.select(flag='twiss', column=['name','s','l',
              'lrad','angle','k1l','k2l','k3l','k1sl','k2sl','k3sl','hkick','vkick','kick','tilt',
              'betx','bety','alfx','alfy','dx','dpx','dy','dpy','mux','muy','x','y','px','py','t','pt',
              'wx','wy','phix','phiy','n1','ddx','ddy','ddpx','ddpy',
              'keyword','aper_1','aper_2','aper_3','aper_4',
              'apoff_1','apoff_2',
              'aptol_1','aptol_2','aptol_3','apertype','mech_sep'])

twiss = madx.twiss()
new_pos_x, aper_neat_x = acc_lib.madx_tools.get_apertures_real(twiss)
new_pos_y, aper_neat_y = acc_lib.madx_tools.get_apertures_real(twiss, axis='vertical')

#%% -------------- Save sequences --------------
#madx.command.save(sequence='sps', file='../SPS_sequence/PS_2021_Protons_thin_matched_with_RF_and_aperture.seq', beam=True)
# MADX does not save file with aperture, has to be added manually! 

# Build Xtrack line importing MAD-X expressions
line = xt.Line.from_madx_sequence(madx.sequence['sps'],
                                  install_apertures=True
                                  )
particle_sample = xp.Particles(
        p0c = madx.sequence.sps.beam.pc*1e9,
        q0 = madx.sequence.sps.beam.charge,
        mass0 = madx.sequence.sps.beam.mass*1e9)

line.particle_ref = particle_sample


# Xsuite - reconfigure RF and correct phase - PROTONS
harmonic_nb = 4620
nn = 'actcse.31637'
V_RF = 3.0 # MV
line[nn].lag = 180  # above transition
line[nn].voltage =  V_RF*1e6 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['sps'].beam.freq0*1e6*harmonic_nb

# Check apertures of line
aper_test = line.check_aperture()

# Save Xsuite sequence
with open('../SPS_sequence/SPS_2021_Protons_matched_with_RF_and_aperture.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)

# Test Twiss to observe apertures
line.build_tracker()
twiss_xtrack = line.twiss()  

#%% Plot the beam envelope and aperture
fig = plt.figure(figsize=(10,7))
ax = acc_lib.madx_tools.plot_envelope(fig, madx, twiss)
acc_lib.madx_tools.plot_apertures_real(ax, new_pos_x, aper_neat_x)
fig.suptitle('SPS Protons - horizontal aperture', fontsize=22)
fig.savefig('Aperture_plots/SPS_2021_Protons_X_aperture.png', dpi=250)

fig2 = plt.figure(figsize=(10,7))
ax2 = acc_lib.madx_tools.plot_envelope(fig2, madx, twiss, axis='vertical')
acc_lib.madx_tools.plot_apertures_real(ax2, new_pos_y, aper_neat_y)
fig2.suptitle('SPS Protons - vertical aperture', fontsize=22)
fig2.savefig('Aperture_plots/SPS_2021_Protons_Y_aperture.png', dpi=250)
