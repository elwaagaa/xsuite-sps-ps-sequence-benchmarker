"""
PS SEQUENCE GENERATOR WITH PB IONS - FLAT BOTTOM

Script to set up PS sequence with MADX using 2022 optics from acc-models/PS
- match tunes and chromaticities exactly as in https://acc-models.web.cern.ch/acc-models/ps/2022/scenarios/lhc_ion/

by Elias Waagaard 
"""

from cpymad.madx import Madx

# Define sequence name and optics to import model from AFS
optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-ps'

# Pb ion beam parameters 
m_u = 931.49410242e6  # 1 Dalton in eV/c^2 -- atomic mass unit 
atomic_mass_pb208 = 207.9766519 # Pb208 atomic mass from The AME2016 atomic mass evaluation (II).
m_ion = atomic_mass_pb208*m_u  
Z = 54
E_kin = 0.0722*1e9*atomic_mass_pb208 # total kinetic energy in eV per particle at injection, from LIU parameter table https://edms.cern.ch/ui/file/1420286/2/LIU-Ions_beam_parameter_table.pdf
E_tot = m_ion + E_kin

# Initiate MADX sequence, and call the relevant madx files and macros  
madx = Madx()
madx.call("{}/_scripts/macros.madx".format(optics))

# This ACC-MODEL BEAM command seems to calculate per particle - but wrong as MADX wants total energy! 
#madx.call("{}/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.beam".format(optics))   
#madx.input('''BRHO      := BEAM->PC * 3.3356 * 208./54.;''')

# Correct beam command tp obtain same Brho and beta0 as Hannes' and Isabelle Table 5 (https://cds.cern.ch/record/2749453)
madx.input(" \
           Beam, particle=ion, mass={}, charge={}, energy = {}; \
           DPP:=BEAM->SIGE*(BEAM->ENERGY/BEAM->PC)^2;  \
           ".format(m_ion/1e9, Z, E_tot/1e9))

# Call main sequence files and magnet strengths  
madx.call("{}/ps_mu.seq".format(optics))
madx.call("{}/ps_ss.seq".format(optics))
madx.call("{}/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.str".format(optics))
madx.use(sequence='ps')

#"""
# Perform a PTC Twiss command 
madx.input(''' 
           use, sequence=PS;
           exec, ptc_twiss_macro(2,0,0);
           ''')
ptc_table = madx.table.ptc_twiss_summary
ptc_twiss = madx.ptc_twiss


# Check against normal Twiss command on thick sequence 
madx.use(sequence='ps')
twiss_thick = madx.twiss()
madx.command.save(sequence='ps', file='PS_2022_Pb_ions_thick.seq', beam=True)  #-> if this is turned on, the chromas become all differenct for thin and thick seq! 

# Perform a Twiss thick after saving the sequence - when it has been flattened
madx.use(sequence='ps')
twiss_thick_after_saving = madx.twiss()

# Slice the sequence
n_slice_per_element = 5
madx.command.select(flag='MAKETHIN', slice=n_slice_per_element, thick=False)
madx.command.makethin(sequence='ps', MAKEDIPEDGE=True)  

# Twiss command of thin sequence 
madx.use(sequence='ps')
twiss_thin = madx.twiss()
madx.command.save(sequence='ps', file='PS_2022_Pb_ions_thin.seq', beam=True)


# Compare thin and thick sequence
beta0 = madx.sequence['ps'].beam.beta
print("PS PB ions: MADX thin vs thick sequence:")
print("MAD-X PTC:    " f"Qx  = {ptc_table['q1'][0]:.8f}"                     f"   Qy = {ptc_table['q2'][0]:.8f}")
print("MAD-X thick:  " f"Qx  = {twiss_thick.summary['q1']:.8f}"           f"   Qy = {twiss_thick.summary['q2']:.8f}")
print("MAD-X thick2: " f"Qx  = {twiss_thick_after_saving.summary['q1']:.8f}"           f"   Qy = {twiss_thick_after_saving.summary['q2']:.8f}")
print("MAD-X thin:   " f"Qx  = {twiss_thin.summary['q1']:.8f}"            f"   Qy = {twiss_thin.summary['q2']:.8f}")
print("\nMAD-X PTC:    " f"Q'x = {ptc_table['dq1'][0]:.8f}"                     f"  Q'y = {ptc_table['dq2'][0]:.8f}")
print("MAD-X thick:  " f"Q'x = {twiss_thick.summary['dq1']*beta0:.7f}"    f"   Q'y = {twiss_thick.summary['dq2']*beta0:.7f}")
print("MAD-X thick2: " f"Q'x = {twiss_thick_after_saving.summary['dq1']*beta0:.7f}"    f"   Q'y = {twiss_thick_after_saving.summary['dq2']*beta0:.7f}")
print("MAD-X thin:   " f"Q'x = {twiss_thin.summary['dq1']*beta0:.7f}"     f"   Q'y = {twiss_thin.summary['dq2']*beta0:.7f}")
print("\nMAD-X PTC:    " f"alpha_p = {ptc_table['alpha_c'][0]:.8f}")
print("MAD-X thick:  " f"alpha_p = {twiss_thick.summary.alfa:.7f}")
print("MAD-X thick2: " f"alpha_p = {twiss_thick_after_saving.summary.alfa:.7f}")
print("MAD-X thin:   " f"alpha_p = {twiss_thin.summary.alfa:.7f}")



""" IF NEEDED TO MATCH
madx.call('PS_match_tunes_and_chroma.madx')
madx.input('''
           qx = 6.21;
           qy = 6.245;
           qpx = -5.27;
           qpy = -7.2;
           
           exec, match_tunes_and_chroma(qx, qy, qpx, qpy);
           ''')
"""