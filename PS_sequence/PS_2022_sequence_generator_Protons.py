"""
PS SEQUENCE GENERATOR WITH PROTONS - FLAT BOTTOM

Script to set up PS sequence with MADX using 2022 optics from acc-models/PS

by Elias Waagaard 
"""

from cpymad.madx import Madx

# Define sequence name and optics to import model from AFS
optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-ps'

# Initiate MADX sequence, and call the relevant madx files and macros  
madx = Madx()
madx.call("{}/_scripts/macros.madx".format(optics))
madx.call("{}/scenarios/lhc/1_flat_bottom/ps_fb_lhc.beam".format(optics))   
madx.input('''BRHO      := BEAM->PC * 3.3356;''')
madx.call("{}/ps_mu.seq".format(optics))
madx.call("{}/ps_ss.seq".format(optics))
madx.call("{}/scenarios/lhc/1_flat_bottom/ps_fb_lhc.str".format(optics))

#"""
# Perform a PTC Twiss command 
madx.input(''' 
           use, sequence=PS;
           exec, ptc_twiss_macro(2,0,0);
           ''')
ptc_table = madx.table.ptc_twiss_summary
ptc_twiss = madx.ptc_twiss


# Perform Twiss with thick-element sequence 
madx.use(sequence='ps')
madx.command.flatten()  # flatten to unnested the combined function magnets 
twiss_thick = madx.twiss()

# Save thick sequence
madx.command.save(sequence='ps', file='PS_2022_Protons_thick.seq', beam=True)

# Perform a Twiss thick after saving the sequence - when it has been flattened
madx.use(sequence='ps')
twiss_thick_after_saving = madx.twiss()

# Slice the sequence and plot madx Twiss
n_slice_per_element = 5
madx.command.select(flag='MAKETHIN', slice=n_slice_per_element, thick=False)
madx.command.makethin(sequence='ps', MAKEDIPEDGE=True)  

# Twiss command of thin sequence 
madx.use(sequence='ps')
twiss_thin = madx.twiss()

# Save the MADX sequence file 
madx.command.save(sequence='ps', file='PS_2022_Protons_thin.seq', beam=True)

# Compare thin and thick sequence
beta0 = madx.sequence['ps'].beam.beta
print("PS Protons: MADX thin vs thick sequence:")
print("MAD-X PTC:    " f"Qx  = {ptc_table['q1'][0]:.8f}"                     f"   Qy = {ptc_table['q2'][0]:.8f}")
print("MAD-X thick:  " f"Qx  = {twiss_thick.summary['q1']:.8f}"           f"   Qy = {twiss_thick.summary['q2']:.8f}")
print("MAD-X thick2: " f"Qx  = {twiss_thick_after_saving.summary['q1']:.8f}"           f"   Qy = {twiss_thick_after_saving.summary['q2']:.8f}")
print("MAD-X thin:   " f"Qx  = {twiss_thin.summary['q1']:.8f}"            f"   Qy = {twiss_thin.summary['q2']:.8f}")
print("\nMAD-X PTC:    " f"Q'x = {ptc_table['dq1'][0]:.8f}"                     f"  Q'y = {ptc_table['dq2'][0]:.8f}")
print("MAD-X thick:  " f"Q'x = {twiss_thick.summary['dq1']*beta0:.7f}"    f"   Q'y = {twiss_thick.summary['dq2']*beta0:.7f}")
print("MAD-X thick2: " f"Q'x = {twiss_thick_after_saving.summary['dq1']*beta0:.7f}"    f"   Q'y = {twiss_thick_after_saving.summary['dq2']*beta0:.7f}")
print("MAD-X thin:   " f"Q'x = {twiss_thin.summary['dq1']*beta0:.7f}"     f"   Q'y = {twiss_thin.summary['dq2']*beta0:.7f}")
print("\nMAD-X PTC:    " f"alpha_p = {ptc_table['alpha_c'][0]:.8f}")
print("MAD-X thick:  " f"alpha_p = {twiss_thick.summary.alfa:.7f}")
print("MAD-X thick2: " f"alpha_p = {twiss_thick_after_saving.summary.alfa:.7f}")
print("MAD-X thin:   " f"alpha_p = {twiss_thin.summary.alfa:.7f}")