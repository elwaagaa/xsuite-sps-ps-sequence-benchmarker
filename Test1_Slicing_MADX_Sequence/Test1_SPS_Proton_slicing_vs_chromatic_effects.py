#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
MADX SLICING TESTS FOR CHROMATIC EFFECTS - SPS Protons
"""
import numpy as np
from cpymad.madx import Madx
import matplotlib.pyplot as plt 

# Initiate MADX instance 
madx = Madx()
madx.call('SPS_sequence/SPS_2021_Protons_thick.seq')

# Perform Twiss with thick-element sequence 
madx.use(sequence='sps')
twiss_thick = madx.twiss()

# Iterate over slice range to compare non-linear effects 
slice_range = np.arange(1, 16, 1)
qx_values = np.zeros(len(slice_range))
qy_values = np.zeros(len(slice_range))
dqx_values = np.zeros(len(slice_range))
dqy_values = np.zeros(len(slice_range))


for i, nr_slices in enumerate(slice_range):
     
    # Reinitiate MADX sequence when it has been already sliced and deleted 
    if not 'madx' in locals():
        madx = Madx()
        madx.call('SPS_sequence/SPS_2021_Protons_thick.seq')

    madx.command.select(flag='MAKETHIN', slice=nr_slices, thick=False)
    madx.command.makethin(sequence='sps', MAKEDIPEDGE=True)  

    # Twiss command of thin sequence 
    madx.use(sequence='sps')
    twiss_thin = madx.twiss()
    
    # Append tunes and chromaticities
    qx_values[i] = twiss_thin.summary['q1']
    qy_values[i] = twiss_thin.summary['q2']
    dqx_values[i] = twiss_thin.summary['dq1']
    dqy_values[i] = twiss_thin.summary['dq2']
    
    del madx
    
# Plot the results 
# Plot parameters
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10,7))
fig.suptitle('SPS Protons - MADX slicing tests')
ax1.plot(slice_range, twiss_thick.summary['q1']*np.ones(len(slice_range)), 
         marker='o', markersize=8,  linestyle=None, markerfacecolor='none', label='Qx Thick sequence')
ax1.plot(slice_range, qx_values, label='Qx Thin sequence')
ax1.legend(fontsize=14)
ax1.set_xticklabels([])
ax1.set_ylabel('Tunes')
ax2.plot(slice_range, twiss_thick.summary['dq1']*np.ones(len(slice_range)), 
         marker='o', markersize=8,  linestyle=None, markerfacecolor='none', label="Qx' Thick sequence")
ax2.plot(slice_range, dqx_values, label="Qx' Thin sequence")
ax2.set_ylabel('Chroma')
ax2.legend(fontsize=14)
ax2.set_xlabel('Nr of slices')
fig.savefig("Test1_plots/SPS_Protons_X_Tunes_and_chroma_vs_slicing.png", dpi=250)

fig2, (ax3, ax4) = plt.subplots(2, 1, figsize=(10,7))
fig2.suptitle('SPS Protons - MADX slicing tests')
ax3.plot(slice_range, twiss_thick.summary['q2']*np.ones(len(slice_range)), 
         marker='o', markersize=8,  linestyle=None, markerfacecolor='none', label='Qy Thick sequence')
ax3.plot(slice_range, qy_values, label='Qy Thin sequence')
ax3.legend(fontsize=14)
ax3.set_xticklabels([])
ax3.set_ylabel('Tunes')
ax4.plot(slice_range, twiss_thick.summary['dq2']*np.ones(len(slice_range)), 
         marker='o', markersize=8,  linestyle=None, markerfacecolor='none', label="Qy' Thick sequence")
ax4.plot(slice_range, dqy_values, label="Qy' Thin sequence")
ax4.set_ylabel('Chroma')
ax4.legend(fontsize=14)
ax4.set_xlabel('Nr of slices')
fig2.savefig("Test1_plots/SPS_Protons_Y_Tunes_and_chroma_vs_slicing.png", dpi=250)