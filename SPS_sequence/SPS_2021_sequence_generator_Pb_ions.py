#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SPS PB ION SEQUENCE GENERATOR 

Script to set up SPS sequence with MADX using 2021 optics from acc-models/SPS
- also comparison between thin and thick MADX sequence
- here we also compare with the PTC Twiss

by Elias Waagaard 
"""

from cpymad.madx import Madx

# Define sequence name and optics to import model from AFS
optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-sps'

#### Initiate MADX sequence and call the sequence and optics file ####
madx = Madx()
madx.call("{}/sps.seq".format(optics))
madx.call("{}/strengths/lhc_ion.str".format(optics))
madx.call("{}/beams/beam_lhc_ion_injection.madx".format(optics))
#madx.call("sps/scenarios/lhc/lhc_ion/job.madx")

# Perform Twiss with thick-element sequence 
madx.use(sequence='sps')
twiss_thick = madx.twiss()

madx.input('''        
dp = 0;
order = 2;
ptc_create_universe;
ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
select, flag=ptc_twiss, clear;
select, flag=ptc_twiss, column=name,keyword,s,x,px,beta11,alfa11,beta22,alfa22,disp1,disp2,mu1,mu2,energy,l,angle,K1L,K2L,K3L,HKICK,SLOT_ID;    

use, sequence=SPS;
ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, normal;
ptc_end;
           ''')
ptc_table = madx.table.ptc_twiss_summary
ptc_twiss = madx.ptc_twiss


# Save thick MADX sequence
madx.command.save(sequence='sps', file='SPS_2021_Pb_ions_thick.seq', beam=True) 

# Perform Twiss with thick-element sequence after saving it
madx.use(sequence='sps')
twiss_thick_after_saving = madx.twiss()

# Slice the sequence and plot madx Twiss
n_slice_per_element = 5
madx.command.select(flag='MAKETHIN', slice=n_slice_per_element, thick=False)
madx.command.makethin(sequence='sps', MAKEDIPEDGE=True)  

# Twiss command of thin sequence 
madx.use(sequence='sps')
twiss_thin = madx.twiss()

# Save the MADX sequence file 
madx.command.save(sequence='sps', file='SPS_2021_Pb_ions_thin.seq', beam=True)

# Compare thin and thick sequence
beta0 = madx.sequence['sps'].beam.beta
print("SPS PB ions: MADX thin vs thick sequence:")
print("MAD-X PTC:   " f"Qx  = {ptc_table['q1'][0]:.8f}"                     f"   Qy = {ptc_table['q2'][0]:.8f}")
print("MAD-X thick:  " f"Qx  = {twiss_thick.summary['q1']:.8f}"           f"   Qy = {twiss_thick.summary['q2']:.8f}")
print("MAD-X thick2: " f"Qx  = {twiss_thick_after_saving.summary['q1']:.8f}"           f"   Qy = {twiss_thick_after_saving.summary['q2']:.8f}")
print("MAD-X thin:   " f"Qx  = {twiss_thin.summary['q1']:.8f}"            f"   Qy = {twiss_thin.summary['q2']:.8f}")
print("\nMAD-X PTC:     " f"Q'x = {ptc_table['dq1'][0]:.8f}"                     f"  Q'y = {ptc_table['dq2'][0]:.8f}")
print("MAD-X thick:  " f"Q'x = {twiss_thick.summary['dq1']*beta0:.7f}"    f"   Q'y = {twiss_thick.summary['dq2']*beta0:.7f}")
print("MAD-X thick2: " f"Q'x = {twiss_thick_after_saving.summary['dq1']*beta0:.7f}"    f"   Q'y = {twiss_thick_after_saving.summary['dq2']*beta0:.7f}")
print("MAD-X thin:   " f"Q'x = {twiss_thin.summary['dq1']*beta0:.7f}"     f"   Q'y = {twiss_thin.summary['dq2']*beta0:.7f}")
print("\nMAD-X PTC:    " f"alpha_p = {ptc_table['alpha_c'][0]:.8f}")
print("MAD-X thick:  " f"alpha_p = {twiss_thick.summary.alfa:.7f}")
print("MAD-X thick2: " f"alpha_p = {twiss_thick_after_saving.summary.alfa:.7f}")
print("MAD-X thin:   " f"alpha_p = {twiss_thin.summary.alfa:.7f}")