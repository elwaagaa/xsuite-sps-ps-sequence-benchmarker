"""
NON-LINEAR CHROMATIC BEHAVIOUR PS WITH Protons

Script to set up ps sequence with MADX using 2021 optics from acc-models/PS, also performing a sanity check between the two,
also generating .json and .seq files with for X-suite use

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json

# Define sequence name and optics to import model from AFS
optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-ps'

# Initiate MADX instance - use proper thick sequence
madx = Madx()
# madx.call('../PS_sequence/PS_2022_Protons_thick.seq')
madx.call("{}/_scripts/macros.madx".format(optics))
madx.call("{}/scenarios/lhc/1_flat_bottom/ps_fb_lhc.beam".format(optics))   
madx.input('''BRHO      := BEAM->PC * 3.3356;''')
madx.call("{}/ps_mu.seq".format(optics))
madx.call("{}/ps_ss.seq".format(optics))
madx.call("{}/scenarios/lhc/1_flat_bottom/ps_fb_lhc.str".format(optics))
madx.use(sequence = 'ps')
twiss_thick = madx.twiss()

# Also do PTC-Twiss 
madx.input('''        
dp = 0;
order = 2;
ptc_create_universe;
ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
select, flag=ptc_twiss, clear;
select, flag=ptc_twiss, column=name,keyword,s,x,px,beta11,alfa11,beta22,alfa22,disp1,disp2,mu1,mu2,energy,l,angle,K1L,K2L,K3L,HKICK,SLOT_ID;    

use, sequence=PS;
ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, normal;
ptc_end;
           ''')
ptc_table = madx.table.ptc_twiss_summary
ptc_twiss = madx.ptc_twiss


#%% Compare non-linear chromatic behaviour of ring 
delta_values = np.arange(-0.006, 0.006, 0.001)
qx_values = np.zeros(len(delta_values))
qy_values = np.zeros(len(delta_values))
qx_values_madx = np.zeros(len(delta_values))
qy_values_madx = np.zeros(len(delta_values))
qx_values_ptc = np.zeros(len(delta_values))
qy_values_ptc = np.zeros(len(delta_values))

for i, delta in enumerate(delta_values):
    print(f"\nWorking on {i} of delta values {len(delta_values)}")

    # MADX thick sequence - for some reason madx.twiss() does not take deltap as input argument    
    madx.input(f'''
               use,sequence=ps;
               twiss, DELTAP={delta};
              ''')
    print(f'MADX Twiss with deltap = {madx.table.summ["deltap"][0]}...')
    qx_values_madx[i] = madx.table.summ['q1'][0]
    qy_values_madx[i] = madx.table.summ['q2'][0]
    
    # Also test the PTC TWISS 
    madx.input(f'''        
    dp = {delta};
    order = 2;
    ptc_create_universe;
    ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
    select, flag=ptc_twiss, clear;
    select, flag=ptc_twiss, column=name,keyword,s,x,px,beta11,alfa11,beta22,alfa22,disp1,disp2,mu1,mu2,energy,l,angle,K1L,K2L,K3L,HKICK,SLOT_ID;    

    use, sequence=PS;
    ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, normal;
    ptc_end;
               ''')
    qx_values_ptc[i] = 6.0+madx.table.ptc_twiss_summary['q1']  # PTC only computes fractional tunes, so add integer
    qy_values_ptc[i] = 6.0+madx.table.ptc_twiss_summary['q2']

#### XSUITE PART ##########

# For some reason slicing the already used sequence does not work - call previous thin sequence
del madx
madx = Madx()
madx.call('../PS_sequence/PS_2022_Protons_thin.seq')

#%% Set up the X-suite contextsTrue
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Create a line for X-suite from MADX sequence 
madx.use(sequence='ps')
line = xt.Line.from_madx_sequence(madx.sequence['ps']) #, apply_madx_errors=True)
madx_beam = madx.sequence['ps'].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample

#%% Perform Twiss command from tracker and save Xtrack sequence in json format
line.build_tracker()
twiss_xtrack = line.twiss(method='4d')  

with open('../PS_sequence/PS_2022_Protons_for_tracking.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)
    
# Iterate over the different delta values
for i, delta in enumerate(delta_values):
    print(f"\nWorking on {i} of delta values {len(delta_values)}")
    # Xtrack
    print("Testing Xtrack twiss...")
    tt = line.twiss(method='4d', delta0=delta)
    qx_values[i] = tt.qx
    qy_values[i] = tt.qy

#####################################################
############ Sanity checks MADX vs Xsuite
#####################################################

# Step 1. Sanity checks: Compare tunes, chromaticities and momentum compation factors
beta0 = line.particle_ref.beta0[0]
print("\nPROTONS: XTRACK vs MADX sequence:")
print("MAD-X PTC:    " f"Qx  = {ptc_table['q1'][0]:.8f}"                     f"   Qy = {ptc_table['q2'][0]:.8f}")
print("MAD-X thick:   " f"Qx  = {twiss_thick.summary['q1']:.8f}"            f"   Qy  = {twiss_thick.summary['q2']:.8f}")
print("Xsuite:       " f"Qx  = {twiss_xtrack['qx']:.8f}"                  f"   Qy  = {twiss_xtrack['qy']:.8f}\n")
print("\nMAD-X PTC:     "f"Q'x = {ptc_table['dq1'][0]:.8f}"                     f"  Q'y = {ptc_table['dq2'][0]:.8f}")
print("MAD-X thick:   " f"Q'x = {twiss_thick.summary['dq1']*beta0:.7f}"     f"   Q'y = {twiss_thick.summary['dq2']*beta0:.7f}")
print("Xsuite:       " f"Q'x = {twiss_xtrack['dqx']:.7f}"                 f"   Q'y = {twiss_xtrack['dqy']:.7f}\n")
print("\nMAD-X PTC:    " f"alpha_p = {ptc_table['alpha_c'][0]:.8f}")
print("MAD-X thick:   " f"alpha_p = {twiss_thick.summary.alfa:.7f}")
print("Xsuite:       " f"alpha_p = {twiss_xtrack['momentum_compaction_factor']:.7f}")

#%% Plot the tunes over delta
# Plot parameters
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12,7))
fig.suptitle('PS Protons: Non-linear chromatic tests')
ax1.plot(delta_values, qx_values, marker='o', label='Xtrack')
ax1.plot(delta_values, qx_values_madx, marker='v', label='MADX Thick')
ax1.plot(delta_values, qx_values_ptc, linestyle='-', label='PTC Thick')
ax1.legend(fontsize=14)
ax1.set_xticklabels([])
ax1.set_ylabel('$Q_{x}$')
ax2.plot(delta_values, qy_values, marker='o',  label='Xtrack')
ax2.plot(delta_values, qy_values_madx, marker='v', label='MADX Thick')
ax2.plot(delta_values, qy_values_ptc, linestyle='-', label='PTC Thick')
ax2.set_ylabel('$Q_{y}$')
ax2.set_xlabel('$\delta$')
fig.savefig("Test2_plots/PS_Protons_nonlinear_chromatic_test.png", dpi=250)

fig2, (ax3, ax4) = plt.subplots(2, 1, figsize=(12,7))
fig2.suptitle('PS Protons: Non-linear chromatic tests')
ax3.plot(delta_values, np.abs(qx_values-qx_values_madx), label='|Xtrack - MADX Thick|')
ax3.plot(delta_values, np.abs(qx_values-qx_values_ptc), label='|Xtrack - PTC Thick|')
ax3.legend(fontsize=14)
ax3.set_xticklabels([])
ax3.set_ylabel('$\delta Q_{x}$')
ax4.plot(delta_values, np.abs(qy_values-qy_values_madx), label='|Xtrack - MADX Thick|')
ax4.plot(delta_values, np.abs(qy_values-qy_values_ptc), label='|Xtrack - PTC Thick|')
ax4.set_ylabel('$\delta Q_{y}$')
ax4.set_xlabel('$\delta$')
fig2.savefig("Test2_plots/PS_Protons_DIFFERENCE_nonlinear_chromatic_test.png", dpi=250)
