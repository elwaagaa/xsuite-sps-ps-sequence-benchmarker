"""
LEIR WITH PB IONS - add RF and test tracking 

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json

# Initiate MADX instance 
madx = Madx()
madx.call('../LEIR_sequence/LEIR_2021_Pb_ions_thin.seq')

# Define parameters for tracking - from LIU table: https://edms.cern.ch/ui/file/1420286/2/LIU-Ions_beam_parameter_table.pdf
num_part = 10000
num_turns = 100
bunch_intensity = 10.1e8  # from Hannes' parameter table at https://cds.cern.ch/record/2749453
sigma_z = 4.256   # also from Hannes' table
nemitt_x= 0.4e-6
nemitt_y= 0.4e-6  

#%% Set up the X-suite contextsTrue
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Create a line for X-suite from MADX sequence 
madx.use(sequence='leir')
line = xt.Line.from_madx_sequence(madx.sequence['leir']) #, apply_madx_errors=True)
madx_beam = madx.sequence['leir'].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample

# Common elements in the two beamlines
common = list(set(line.element_names).intersection(madx.sequence.leir.element_names()))


#%% SET CAVITY VOLTAGE - with info from Nicolo Biancacci and LSA
# Ions: we set for nominal cycle V_RF = 3.2 kV and h = 2
# Two cavities: ER.CRF41 and ER.CRF43 - we use the first of them
harmonic_nb = 2
nn = 'er.crf41' # for now test the first of the RF cavities 
V_RF = 3.2  # kV

# MADX sequence 
madx.sequence.leir.elements[nn].lag = 0
madx.sequence.leir.elements[nn].volt = V_RF*1e-3*particle_sample.q0 # different convention between madx and xsuite
madx.sequence.leir.elements[nn].freq = madx.sequence['leir'].beam.freq0*harmonic_nb

# Xsuite sequence 
line[nn].lag = 0  # 0 if below transition
line[nn].voltage =  V_RF*1e3 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['leir'].beam.freq0*1e6*harmonic_nb

# Save sequence with RF installed 
with open('../LEIR_sequence/LEIR_2021_Pb_ions_with_RF.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)


#%% XSUITE TRACKING 

# Perform a Twiss command
line.build_tracker()
twiss_xtrack = line.twiss()

# Generate matched gaussian distribution of particles and track them 
particles_2 = xp.Particles(mass0=madx_beam.mass*1e9, gamma0=madx_beam.gamma, q0 = madx_beam.charge, 
                           p0c = madx_beam.pc*1e9,
                           x=[1e-6], px=[1e-6], y=[1e-6], py=[1e-6],
                           zeta=[0], delta=[0])

# Track the particles
line.track(particles_2, turn_by_turn_monitor='ONE_TURN_EBE')

df_track = pd.DataFrame({"s": line.record_last_track.s[0][:-1],
                         "x": line.record_last_track.x[0][:-1],
                         "ptau": line.record_last_track.ptau[0][:-1],
                         "delta":line.record_last_track.delta[0][:-1], 
                         "name": line.element_names })

df_track = df_track[df_track.name.isin(common) ]

#%% PTC TRACKING 
# For the PTC, remember nst determines slicing (for xtrack it is for, but nst=10 does not make a big difference)
madx.input(f'''
ptc_create_universe;
ptc_create_layout,model=2,method=6,nst=10,exact;

ptc_start, x= 1e-6, px=1e-6, y= 1e-6, py=1e-6;
''')

# Put marker for PTC observation points
for element in common:
    madx.input(f'''
    ptc_observe, PLACE={element};
    ''')
madx.input(f'''
ptc_track,icase=6,closed_orbit, element_by_element, ,dump,
       turns= 1,ffile=1, onetable;
ptc_track_end;
ptc_end;
''')

# Extract data to CSV File 
ptc_track = pd.read_csv( "trackone", delim_whitespace=True, skiprows=9, header=None)
ptc_track = ptc_track[ptc_track[0].str.contains('segment') == False]
ptc_track.columns = ["NUMBER","TURN","X","PX","Y","PY","T", "PT", "S", "E"]

#%% Test also to track a Gaussian bunch for 100 turns to observe longitudinal phase space 
particles0 = xp.generate_matched_gaussian_bunch(
         num_particles=num_part, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=line.particle_ref, line=line)

# Track the particles
line.track(particles0, num_turns = num_turns, turn_by_turn_monitor=True)

#%% Plot the tunes over delta
# Plot parameters
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fig, ax = plt.subplots(nrows=2, figsize=(10, 8))
fig.suptitle("LEIR PB Ion Tracking: PTC vs Xtrack", fontsize=22)
plt.sca(ax[0])
plt.plot(ptc_track.S.values, ptc_track.X.values, label='PTC', marker='o', linestyle=None, ms=6)
plt.plot(df_track.s, df_track.x, label='XTRACK')
plt.legend()
plt.ylabel("x")
plt.sca(ax[1])
plt.plot(ptc_track.S.values[1:-1], ptc_track.X.values[1:-1]-df_track.x.values, label='PTC', marker='o', ms=3, color='r')
plt.xlabel("s (m)")
plt.ylabel("Delta x")
fig.savefig("Test3_Plots/LEIR_PB_Ions_comparison_PTC_Xsuite.png", dpi=250)

fig2, ax2 = plt.subplots(nrows=2, figsize=(10, 8))
fig2.suptitle("LEIR PB Ion Tracking: PTC vs Xtrack", fontsize=22)
plt.sca(ax2[0])
plt.plot(ptc_track.S.values, ptc_track.PT.values, label='PT from PTC', marker='o', ms=3)
plt.plot(df_track.s, df_track.ptau, label='ptau from XTRACK', marker='o', ms=3)
plt.legend()
plt.ylabel("PT and Ptau")
plt.sca(ax2[1])
plt.plot(ptc_track.S.values[1:-1], ptc_track.PT.values[1:-1]-df_track.ptau.values, label='PTC', marker='o', ms=3, color='r')
#ax2[0].set_ylim(8.259713e-10, 8.259716e-10)  # to compare small offset
#ax2[0].set_xlim(2800, 2855)  # to compare small offset
#ax2[1].set_xlim(2800, 2855)
plt.xlabel("s (m)")
plt.ylabel("|PT - DeltaP|")
fig2.savefig("Test3_Plots/LEIR_PB_Ions_comparison_PTau.png", dpi=250)

# Also plot the longitudinal phase space 
fig3, ax3 = plt.subplots(1, 1, figsize = (10,5))
fig3.suptitle('LEIR PB ion tracking: Longitudinal Phase Space')
ax3.plot(particles0.zeta, particles0.delta*1000, '.', markersize=3)
ax3.set_xlabel(r'z [-]')
ax3.set_ylabel(r'$\delta$ [1e-3]')
fig3.savefig("Test3_Plots/LEIR_PB_Ions_Longitudinal_Tracking.png", dpi=250)