"""
SPS WITH PROTON - add RF and test tracking 

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json

# Initiate MADX instance 
madx = Madx()
madx.call('../SPS_sequence/SPS_2021_Protons_thin.seq')
madx.use(sequence = 'sps')
twiss_thin = madx.twiss()

# Define parameters for tracking (Proton ions intensities and emittances from Hannes' and Isabelle's table)
bunch_intensity = 1e11/3
sigma_z = 22.5e-2/3 # Relatively short bunch 
nemitt_x=2.5e-6
nemitt_y=2.5e-6

#%% Set up the X-suite contextsTrue
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Create a line for X-suite from MADX sequence 
madx.use(sequence='sps')
line = xt.Line.from_madx_sequence(madx.sequence['sps']) #, apply_madx_errors=True)
madx_beam = madx.sequence['sps'].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample

# Common elements in the two beamlines
common = list(set(line.element_names).intersection(madx.sequence.sps.element_names()))

#%% SET CAVITY VOLTAGE - with info from Hannes
# 6x200 MHz cavities: actcse, actcsf, actcsh, actcsi (3 modules), actcsg, actcsj (4 modules)
# acl 800 MHz cavities
# acfca crab cavities
# Ions: all 200 MHz cavities: 1.7 MV, h=4653
harmonic_nb = 4620
nn = 'actcse.31637'
V_RF = 3.0 # MV

# MADX sequence 
madx.sequence.sps.elements[nn].lag = 180 # above transition
madx.sequence.sps.elements[nn].volt = V_RF*particle_sample.q0 # different convention between madx and xsuite
madx.sequence.sps.elements[nn].freq = madx.sequence['sps'].beam.freq0*harmonic_nb

# Xsuite sequence 
line[nn].lag = 180  # above transition
line[nn].voltage =  V_RF*1e6 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['sps'].beam.freq0*1e6*harmonic_nb

# Save sequence with RF installed 
with open('../SPS_sequence/SPS_2021_Protons_with_RF.json', 'w') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)

#%% XSUITE TRACKING 

# Generate matched gaussian distribution of particles and track them 
particles_2 = xp.Particles(mass0=madx_beam.mass*1e9, gamma0=madx_beam.gamma, q0 = madx_beam.charge, 
                           p0c = madx_beam.pc*1e9,
                           x=[1e-6], px=[1e-6], y=[1e-6], py=[1e-6],
                           zeta=[0], delta=[0])

# Track the particles
line.build_tracker()
line.track(particles_2, turn_by_turn_monitor='ONE_TURN_EBE')

df_track = pd.DataFrame({"s": line.record_last_track.s[0][:-1],
                         "x": line.record_last_track.x[0][:-1],
                         "ptau": line.record_last_track.ptau[0][:-1],
                         "delta":line.record_last_track.delta[0][:-1], 
                         "name": line.element_names })

df_track = df_track[df_track.name.isin(common) ]

#%% PTC TRACKING 
# For the PTC, remember nst determines slicing (for xtrack it is for, but nst=10 does not make a big difference)
madx.input(f'''
ptc_create_universe;
ptc_create_layout,model=2,method=6,nst=10,exact;

ptc_start, x= 1e-6, px=1e-6, y= 1e-6, py=1e-6;
''')

# Put marker for PTC observation points
for element in common:
    madx.input(f'''
    ptc_observe, PLACE={element};
    ''')
madx.input(f'''
ptc_track,icase=6,closed_orbit, element_by_element, ,dump,
       turns= 1,ffile=1, onetable;
ptc_track_end;
ptc_end;
''')

# Extract data to CSV File 
ptc_track = pd.read_csv( "trackone", delim_whitespace=True, skiprows=9, header=None)
ptc_track = ptc_track[ptc_track[0].str.contains('segment') == False]
ptc_track.columns = ["NUMBER","TURN","X","PX","Y","PY","T", "PT", "S", "E"]

#%% Plot the tunes over delta
# Plot parameters
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fig, ax = plt.subplots(nrows=2, figsize=(10, 8))
fig.suptitle("SPS Proton Tracking: PTC vs Xtrack", fontsize=22)
plt.sca(ax[0])
plt.plot(ptc_track.S.values, ptc_track.X.values, label='PTC', marker='o', linestyle=None, ms=6)
plt.plot(df_track.s, df_track.x, label='XTRACK')
plt.legend()
plt.ylabel("x")
plt.sca(ax[1])
plt.plot(ptc_track.S.values[1:-1], ptc_track.X.values[1:-1]-df_track.x.values, label='PTC', marker='o', ms=3, color='r')
plt.xlabel("s (m)")
plt.ylabel("Delta x")
fig.savefig("Test3_Plots/SPS_Proton_comparison_PTC_Xsuite.png", dpi=250)

fig2, ax2 = plt.subplots(nrows=2, figsize=(10, 8))
fig2.suptitle("SPS Proton Tracking: PTC vs Xtrack", fontsize=22)
plt.sca(ax2[0])
plt.plot(ptc_track.S.values, ptc_track.PT.values, label='PT from PTC', marker='o', ms=3)
plt.plot(df_track.s, df_track.ptau, label='ptau from XTRACK', marker='o', ms=3)
plt.legend()
plt.ylabel("PT and Ptau")
plt.sca(ax2[1])
plt.plot(ptc_track.S.values[1:-1], ptc_track.PT.values[1:-1]-df_track.ptau.values, label='PTC', marker='o', ms=3, color='r')
#ax2[0].set_ylim(8.259713e-10, 8.259716e-10)  # to compare small offset
#ax2[0].set_xlim(2800, 2855)  # to compare small offset
#ax2[1].set_xlim(2800, 2855)
plt.xlabel("s (m)")
plt.ylabel("|PT - DeltaP|")