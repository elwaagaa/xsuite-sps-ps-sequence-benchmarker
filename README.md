# Xsuite vs MADX - non-linear chromatic tests

Smaller accelerators in particular can have non-linear physics phenomena and fringe fields, which can disappear with `makethin` commands in MADX going from thick to thin sequences. This repository investigates to which extent the established tracking code MADX and in particular its PTC module agree with the Xsuite tracking code - also including radiofrequency cavities (RF) and the longitudinal plane. We do this for protons and Pb ions, for the Super Proton Synchrotron (SPS) and for the Proton Synchrotron (PS).

We perform tests to 
a) check how tunes and chromaticities change depending on the number of slices in the MADX `makethin` command   
b) investigate the non-linear chromatic behaviour of ring with a Twiss command and compare for different values of momentum spread $\delta$
c) add RF cavities and compare single-particle tracking 
d) matching chromaticities for the PS with its pole face windings


## Test 1: MADX slicing effects

Plot tunes and chromaticies over different number of slices for making a thin MADX sequence. For the X-plane for Pb ions in the SPS we observe this trend:

![](Test1_Slicing_MADX_Sequence/Test1_plots/SPS_Pb_ions_X_Tunes_and_chroma_vs_slicing.png)

which agrees with the slicing observed also for protons. Also in the PS we see this trend. 

#### Conclusion
The difference between all the thick and thin sequences in all cases is *zero*, so by also using a few number of slices we can trust the thin SPS sequence both for protons and ions. Keeping a default number of slices = 5 should be fine. 

## Test 2: Non-linear chromatic tests 

We first investigate the difference of tunes, chromaticity and momentum compaction factor between 4D Twiss commands for the thin MADX sequence and the Xsuite sequence. We plot tunes and chromaticies over different values of momentum spread $\delta$ to observe to what extent the non-linearities appear in the two cases. The Pb ions case for the SPS is representative also for the other cases:

```
SPS PB IONS: XTRACK vs MADX sequence:
MAD-X PTC:    Qx  = 0.29936474    Qy  = 0.24936295
MAD-X thin:   Qx  = 26.29936474   Qy  = 26.24936295
Xsuite:       Qx  = 26.29937059   Qy  = 26.24936295


MAD-X PTC:    Q'x = -0.13569915  Q'y = -0.09261155
MAD-X thin:   Q'x = -0.1356991   Q'y = -0.0926115
Xsuite:       Q'x = -0.1357073   Q'y = -0.0926205


MAD-X PTC:    alpha_p = 0.00190098
MAD-X thin:   alpha_p = 0.0019010
Xsuite:       alpha_p = 0.0019010
```
![](Test2_Nonlinear_Chromatic_Behaviour/Test2_plots/SPS_Pb_ions_nonlinear_chromatic_test.png)
![](Test2_Nonlinear_Chromatic_Behaviour/Test2_plots/SPS_Pb_ions_DIFFERENCE_nonlinear_chromatic_test.png)


#### Conclusions
We observe differences of order `1e-6` for the tunes for all values of $\delta$ between Xsuite and PTC - the MADX Twiss seem to be slightly more unreliable. Xsuite and MADX-PTC agree, but there is a difference in chromaticity between PTC and MADX. 

## Test 3: add RF and test tracking

For the **SPS** case, we follow the same straightforward procedure as on the [Xsuite SPS ion example](https://github.com/xsuite/xtrack/blob/main/test_data/sps_ions/000_make_line.py) with the updated harmonics and RF voltages as given by Alexandre Lasheen, which for ions are:
```
# SET CAVITY VOLTAGE - with info from Hannes
# 6x200 MHz cavities: actcse, actcsf, actcsh, actcsi (3 modules), actcsg, actcsj (4 modules)
# acl 800 MHz cavities
# acfca crab cavities
# Ions: all 200 MHz cavities: 1.7 MV, h=4653
harmonic_nb = 4653
nn = 'actcse.31632'

# MADX sequence 
madx.sequence.sps.elements[nn].lag = 0
madx.sequence.sps.elements[nn].volt = 3.0*particle_sample.q0 # different convention between madx and xsuite
madx.sequence.sps.elements[nn].freq = madx.sequence['sps'].beam.freq0*harmonic_nb

# Xsuite sequence 
line[nn].lag = 0  # 0 if below transition
line[nn].voltage =  3.0e6 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['sps'].beam.freq0*1e6*harmonic_nb
```
Tracking over one turn for a single particle yields:

![](Test3_AddRF_and_Test_Tracking/Test3_Plots/SPS_PB_Ions_comparison_PTC_Xsuite.png)

with similar results for protons. 

The **PS** is a bit more difficult as no example for Xsuite yet exists. After some detective work to find the correct 10 MHz RF units in the MADX sequence and checking on LSA for Pb ion cycles in the PS, we configure the RF for the PS ions:

```
SET CAVITY VOLTAGE - with info from Alexandre Lasheen
# Ions: 10 MHz cavities: 1.7 MV, h=16
# In the ps_ss.seq, the 10 MHz cavities are PR_ACC10 - there seems to be 12 of them in the straight sections
harmonic_nb = 16
nn = 'pa.c10.11'  # for now test the first of the RF cavities 
V_RF = 38.0958  # kV

# MADX sequence 
madx.sequence.ps.elements[nn].lag = 0
madx.sequence.ps.elements[nn].volt = V_RF*1e-3*particle_sample.q0 # different convention between madx and xsuite
madx.sequence.ps.elements[nn].freq = madx.sequence['ps'].beam.freq0*harmonic_nb

# Xsuite sequence 
line[nn].lag = 0  # 0 if below transition
line[nn].voltage =  V_RF*1e3 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence['ps'].beam.freq0*1e6*harmonic_nb
```

![](Test3_AddRF_and_Test_Tracking/Test3_Plots/PS_PB_Ions_comparison_PTC_Xsuite.png)

We can also check the longitudinal phase space of 10000 particles after 100 turns. If we now track a matched Gaussian bunch with the nominal PS parameters for Pb ions
```
num_part = 10000
num_turns = 100
bunch_intensity = 8.1e8
sigma_z = 4.74
nemitt_x= 0.8e-6
nemitt_y= 0.5e-6  
```
we observe the longitudinal phase space after 100 turns. No particle is lost and they all appear to be in the correct bucket.  

![](Test3_AddRF_and_Test_Tracking/Test3_Plots/PS_PB_Ions_Longitudinal_Tracking.png)

## Test 4: matching chromaticities for the PS 

The PS chromaticity becomes quite different after the `makethin` command. We use the pole face windings to rematch the chromaticity to the original correct values as found in the [acc-model PS ion table]( https://acc-models.web.cern.ch/acc-models/ps/2022/scenarios/lhc_ion/)
- We use macro `PS_match_tunes_and_chroma.madx` inspired by Sebastien Joly
- Be careful with the MADX convention of all chromatic functions which are multiplied by the relativistic beta - to get the right chromaticities and tunes comparing PTC, MADX and Xsuite: 

```
beta0 = madx.sequence['ps'].beam.beta 
madx.input("qx = 6.21")
madx.input("qy = 6.245")
madx.input(f"qpx = -5.26716824/{beta0}")
madx.input(f"qpy = -7.199251093/{beta0}")
madx.input("exec, match_tunes_and_chroma(qx, qy, qpx, qpy);")
```

Which gives us the output:
```
PB IONS: XTRACK vs MADX sequence:
MAD-X PTC:    Qx  = 0.21000000   Qy = 0.24500000
MAD-X thin:   Qx  = 6.20999926   Qy  = 6.24500000
Xsuite:       Qx  = 6.21000009   Qy  = 6.24499999


MAD-X PTC:    Q'x = -5.26716824  Q'y = -7.19925109
MAD-X thin:   Q'x = -5.2642416   Q'y = -7.1952898
Xsuite:       Q'x = -5.2671952   Q'y = -7.1992492


MAD-X PTC:    alpha_p = 0.02701322
MAD-X thin:   alpha_p = 0.0274726
Xsuite:       alpha_p = 0.0270132
```

We then install the RF cavity to the matched sequence and observe whether these values are still the same:
```
PB IONS WITH RF: XTRACK vs MADX sequence:
MAD-X thin:   Qx  = 6.20999926   Qy  = 6.24500000
Xsuite:       Qx  = 6.21000051   Qy  = 6.24499999

MAD-X thin:   Q'x = -5.2642416   Q'y = -7.1952898
Xsuite:       Q'x = -5.2645560   Q'y = -7.1952138

MAD-X thin:   alpha_p = 0.0274726
Xsuite:       alpha_p = 0.0269571
```
where we see good agreement between PTC and Xsuite - PS Pb ion sequence at flat bottom is ready to be used for tracking and space charge studies! 

