"""
PS PB IONS - CHECK MADX GROWTH RATES FOR DIFFERENT ENERGIES
"""
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json
from injector_model import InjectorChain_full_SC # can be installted with "pip install -e injector_model"
# from this repo: https://github.com/ewaagaard/injector_model/tree/main

# Instantiate the injector chain object 
ion_type = 'Pb'
Nq = 54.
injectors = InjectorChain_full_SC(ion_type)
injectors.init_ion(ion_type)
injectors.simulate_injection()
use_RF = True

# Input beam parameters at SPS injection - approximate values from 12/09/2023 measurements
exn = 0.8e-6
eyn = 0.5e-6
dpp = 0.63e-3
sigma_z = 6.0 # at injection 
bunch_intensity = 7.0e8

optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-ps'

# Pb ion beam parameters 
m_u = 931.49410242e6  # 1 Dalton in eV/c^2 -- atomic mass unit 
atomic_mass_pb208 = 207.9766519 # Pb208 atomic mass from The AME2016 atomic mass evaluation (II).
m_ion = atomic_mass_pb208*m_u  
Z = 54
E_kin = 0.0722*1e9*atomic_mass_pb208 # total kinetic energy in eV per particle at injection, from LIU parameter table https://edms.cern.ch/ui/file/1420286/2/LIU-Ions_beam_parameter_table.pdf
E_tot = m_ion + E_kin

# Initiate MADX sequence, and call the relevant madx files and macros  
madx = Madx()
madx.call("{}/_scripts/macros.madx".format(optics))

# Correct beam command tp obtain same Brho and beta0 as Hannes' and Isabelle Table 5 (https://cds.cern.ch/record/2749453)
madx.input(" \
           Beam, particle=ion, mass={}, charge={}, energy = {}; \
           DPP:=BEAM->SIGE*(BEAM->ENERGY/BEAM->PC)^2;  \
           ".format(m_ion/1e9, Z, E_tot/1e9))

madx.call("{}/ps_mu.seq".format(optics))
madx.call("{}/ps_ss.seq".format(optics))
madx.call("{}/scenarios/lhc_ion/1_flat_bottom/ps_fb_ion.str".format(optics))
madx.call('../PS_sequence/PS_match_tunes_and_chroma.madx')
madx.use(sequence='ps')

# Put in values for PS ions: https://acc-models.web.cern.ch/acc-models/ps/2022/scenarios/lhc_ion/

# When we perform matching, recall the MADX convention that all chromatic functions are multiplied by relativistic beta factor
# Thus, when we match for a given chromaticity, need to account for this factor to get correct value in Xsuite and PTC
beta0 = madx.sequence['ps'].beam.beta 
madx.input("qx = 6.21")
madx.input("qy = 6.245")
madx.input(f"qpx = -5.26716824/{beta0}")
madx.input(f"qpy = -7.199251093/{beta0}")

madx.use("ps")
madx.input("seqedit, sequence=PS;")
madx.input("flatten;")
madx.input("endedit;")
madx.use("ps")
madx.input("select, flag=makethin, slice=5, thick=false;")
madx.input("makethin, sequence=ps, style=teapot, makedipedge=True;")
madx.use('ps')
madx.input("exec, match_tunes_and_chroma(qx, qy, qpx, qpy);")

# Provide new input parameters for beam
madx.sequence.ps.beam.exn = exn
madx.sequence.ps.beam.eyn = eyn
madx.sequence.ps.beam.sigt = sigma_z
madx.sequence.ps.beam.sige = dpp*(madx.sequence.ps.beam.beta)**2
madx.sequence.ps.beam.npart = bunch_intensity

madx.use(sequence = 'ps')
twiss_thin = madx.twiss()

# MADX IBS module
madx.input(f'''
!show, beam;
ibs;

! Beam size growth time
Tx=1/ibs.tx;
Ty=1/ibs.ty;
Tl=1/ibs.tl;
''')
Tx = madx.globals.Tx
Ty = madx.globals.Ty
Tl = madx.globals.Tl

####### IBS GROWTH RATE FROM XSUITE #####

# First convert MADX sequence to Xsuite
line = xt.Line.from_madx_sequence(madx.sequence['ps']) #, apply_madx_errors=True)
madx_beam = madx.sequence['ps'].beam

particle_ref = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_ref
line.build_tracker()
twiss_xsuite = line.twiss(method='4d')


# Calculate growth rate with Xsuite analytical model
# --> from Eq. 4 in M. Zampetakis paper: http://arxiv.org/abs/2308.02196
# remember that deps/dt = 2 / T_IBS * eps, so we need to divide the Xsuite growth rate with 2 in this case
Tx_2, Ty_2, Tl_2, kinX, kinY, kinZ = injectors.find_analytical_IBS_growth_rates(
                                                             particle_ref,
                                                             twiss_xsuite,
                                                             line,
                                                             bunch_intensity, 
                                                             sigma_z,
                                                             exn,
                                                             eyn,
                                                             dpp, 
                                                             calculate_kinetic_coefficients=True
                                                             )
# Also try with the built-in SPS IBS growth rate calculator in the injector model
Tx_3, Ty_3, Tl_3 = injectors.calculate_IBS_growth_rate_for_PS(bunch_intensity, madx.sequence.ps.beam.gamma, sigma_z)

print('\nPS:\n')
print('\nMADX IBS module: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(Tx, Ty, Tl))
print('\nXSUITE analytical IBS module: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(Tx_2, Ty_2, Tl_2))
print('\nXSUITE kinetic IBS module: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(kinX, kinY, kinZ))
print('\nINJECTOR MODEL analytical IBS: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(Tx_3, Ty_3, Tl_3))