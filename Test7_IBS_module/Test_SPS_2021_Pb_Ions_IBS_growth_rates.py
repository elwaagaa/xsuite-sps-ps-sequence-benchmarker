"""
SPS PB IONS - CHECK MADX GROWTH RATES FOR DIFFERENT ENERGIES
"""
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json
from injector_model import InjectorChain_full_SC # can be installted with "pip install -e injector_model"
# from this repo: https://github.com/ewaagaard/injector_model/tree/main

# Instantiate the injector chain object 
ion_type = 'Pb'
Nq = 82.
injectors = InjectorChain_full_SC(ion_type)
injectors.init_ion(ion_type)
injectors.simulate_injection()
use_RF = True

# Input beam parameters at SPS injection - approximate values from 12/09/2023 measurements
exn = 1.3e-6
eyn = 0.9e-6
dpp = 1e-3
sigma_z = 0.26 
bunch_intensity = 2.8e8

# Define tunes and chromaticity
qx0 = 26.30
qy0 = 26.25
dq1 = -3.e-9
dq2 = -3.e-9

optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-sps'

#### Initiate MADX sequence and call the sequence and optics file ####
madx = Madx()
madx.call("{}/sps.seq".format(optics))
madx.call("{}/strengths/lhc_ion.str".format(optics))
madx.call("{}/beams/beam_lhc_ion_injection.madx".format(optics))
madx.use(sequence='sps')

# Make thin sequence
madx.use("sps")
madx.input("seqedit, sequence=SPS;")
madx.input("flatten;")
madx.input("endedit;")
madx.use("sps")
madx.input("select, flag=makethin, slice=5, thick=false;")
madx.input("makethin, sequence=sps, style=teapot, makedipedge=True;")

# Use correct tune and chromaticity matching macros
madx.call("{}/toolkit/macro.madx".format(optics))
madx.use('sps')
madx.exec(f"sps_match_tunes({qx0},{qy0});")
madx.exec("sps_define_sext_knobs();")
madx.exec("sps_set_chroma_weights_q26();")
madx.input(f"""match;
global, dq1={dq1};
global, dq2={dq2};
vary, name=qph_setvalue;
vary, name=qpv_setvalue;
jacobian, calls=10, tolerance=1e-25;
endmatch;""")

# Provide new input parameters for beam
madx.sequence.sps.beam.exn = exn
madx.sequence.sps.beam.eyn = eyn
madx.sequence.sps.beam.sigt = sigma_z
madx.sequence.sps.beam.sige = dpp*(madx.sequence.sps.beam.beta)**2
madx.sequence.sps.beam.npart = bunch_intensity

# Twiss command of thin sequence 
madx.use(sequence='sps')
twiss_thin = madx.twiss()    

# MADX IBS module
madx.input(f'''
!show, beam;
ibs;

! Beam size growth time
Tx=1/ibs.tx;
Ty=1/ibs.ty;
Tl=1/ibs.tl;
''')
Tx = madx.globals.Tx
Ty = madx.globals.Ty
Tl = madx.globals.Tl

####### IBS GROWTH RATE FROM XSUITE #####

# First convert MADX sequence to Xsuite
line = xt.Line.from_madx_sequence(madx.sequence['sps']) #, apply_madx_errors=True)
madx_beam = madx.sequence['sps'].beam

particle_ref = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_ref

# DOES NOT SEEM TO BE A DIFFERENCE FOR IBS GROWTH RATES WITH OR WITHOUT RF
if use_RF:
    # SET CAVITY VOLTAGE - with info from Hannes
    # 6x200 MHz cavities: actcse, actcsf, actcsh, actcsi (3 modules), actcsg, actcsj (4 modules)
    # acl 800 MHz cavities
    # acfca crab cavities
    # Ions: all 200 MHz cavities: 1.7 MV, h=4653
    harmonic_nb = 4653
    nn = 'actcse.31632'
    
    # MADX sequence 
    madx.sequence.sps.elements[nn].lag = 0
    madx.sequence.sps.elements[nn].volt = 3.0*particle_ref.q0 # different convention between madx and xsuite
    madx.sequence.sps.elements[nn].freq = madx.sequence['sps'].beam.freq0*harmonic_nb
    
    # Xsuite sequence 
    line[nn].lag = 0  # 0 if below transition
    line[nn].voltage =  3.0e6 # In Xsuite for ions, do not multiply by charge as in MADX
    line[nn].frequency = madx.sequence['sps'].beam.freq0*1e6*harmonic_nb

    line.build_tracker()
    twiss_xsuite = line.twiss(method='6d')

else: 
    line.build_tracker()
    twiss_xsuite = line.twiss(method='4d')


# Calculate growth rate with Xsuite analytical model
# --> from Eq. 4 in M. Zampetakis paper: http://arxiv.org/abs/2308.02196
# remember that deps/dt = 2 / T_IBS * eps, so we need to divide the Xsuite growth rate with 2 in this case
Tx_2, Ty_2, Tl_2, kinX, kinY, kinZ = injectors.find_analytical_IBS_growth_rates(
                                                             particle_ref,
                                                             twiss_xsuite,
                                                             line,
                                                             bunch_intensity, 
                                                             sigma_z,
                                                             exn,
                                                             eyn,
                                                             dpp, 
                                                             calculate_kinetic_coefficients=True
                                                             )

# Also try with the built-in SPS IBS growth rate calculator in the injector model
Tx_3, Ty_3, Tl_3 = injectors.calculate_IBS_growth_rate_for_SPS(bunch_intensity, madx.sequence.sps.beam.gamma, sigma_z)

print('\nMADX IBS module: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(Tx, Ty, Tl))
print('\nXSUITE analytical IBS module: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(Tx_2, Ty_2, Tl_2))
print('\nXSUITE kinetic IBS module: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(kinX, kinY, kinZ))
print('\nINJECTOR MODEL analytical IBS: \nTx = {}, \nTy = {}, \nTl = {}\n'.format(Tx_3, Ty_3, Tl_3))